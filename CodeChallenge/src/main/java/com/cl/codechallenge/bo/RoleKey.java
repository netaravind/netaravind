/**
 * 
 */
package com.cl.codechallenge.bo;

/**
 * This enum is used a key to refer the list of Roles configured in the Organization
 * 
 * @author Aravind (netaravind@gmail.com)
 * @see com.cl.codechallenge.bo.impl.Organization
 */
public enum RoleKey {
    MANAGER, DEVELOPER, QA;
}
