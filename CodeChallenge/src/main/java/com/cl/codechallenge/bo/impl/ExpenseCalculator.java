/**
 * 
 */
package com.cl.codechallenge.bo.impl;

import java.math.BigDecimal;
import java.text.NumberFormat;

import com.cl.codechallenge.bo.Department;
import com.cl.codechallenge.bo.Employee;
import com.cl.codechallenge.bo.ExpenseReport;
import com.cl.codechallenge.bo.ExpenseVO;

/**
 * This class contains the core logic for Expense Allocation calculation. This contain methods to calculate Expense Allocation based on Employee or
 * Department and calculation can be go up to N level.
 * 
 * @author Aravind (netaravind@gmail.com)
 * @see com.cl.codechallenge.bo.Employee
 * @see com.cl.codechallenge.bo.Department
 * @see com.cl.codechallenge.bo.ExpenseVO
 * @see com.cl.codechallenge.bo.ExpenseReport
 *
 */
public class ExpenseCalculator {

    /**
     * This is the method to calculate the expense allocation based on the employee. This method will be called in a recursive fashion until the Level
     * passed in the input
     * 
     * @param employee
     *            Employee object whose Expense Allocation Report to be generated
     * @param expenseVO
     *            Used to hold the calculated Expense Amount, Detailed Employee Information generated during the runtime
     * @param level
     *            Used to specify the Depth of level of Employee hierarchy used for Report generation
     * @return ExpenseVO Contains the calculated Expense Allocation, Detailed Employee Information
     */
    private ExpenseVO calculateExpense(Employee employee, ExpenseVO expenseVO, int level) {
        if (isLevelNotReached(expenseVO, level++)) {
            expenseVO.addExpenseTotal(employee.getExpenseAllocation());
            expenseVO.setDetailedReport(String.format("Employee Name: %s %s ==> Expense Allocation: %s ==> Running Total: %s %n",
                    employee.getLastName(), employee.getFirstName(), getFormatedExpAlloc(employee.getExpenseAllocation()),
                    expenseVO.getExpenseTotal()));
            for (Employee repotee : employee.getDirectReports()) {
                calculateExpense(repotee, expenseVO, level);
            }
        }
        return expenseVO;
    }

    /**
     * This method used to determine the Level is reached or not
     * 
     * @param expenseVO
     * @param level
     * @return True or False to indicate the level is not reached or reached
     */
    private boolean isLevelNotReached(ExpenseVO expenseVO, int level) {
        boolean isLevelNotReached = false;
        if (expenseVO.getLevel() == 0 || expenseVO.getLevel() >= level)
            isLevelNotReached = true;
        return isLevelNotReached;
    }

    /**
     * This method is used for Expense Allocation calculation based on Employee
     * 
     * @param employee
     *            Expense Allocation to be calculated
     * @param report
     *            Used to print the report in different formats based on the implementation i.e., Strategy Pattern
     * @param level
     *            Employee hierarchy Level of depth to be used for Expense Allocation calculation
     * @return Total Expense Allocation Amount in Currency format
     */
    public String calculateExpense(Employee employee, ExpenseReport report, int level) {
        ExpenseVO expenseVO = new ExpenseVO();
        expenseVO.setLevel(level);
        return report.generateExpenseReport(calculateExpense(employee, expenseVO, 0));
    }

    /**
     * This method is used for Expense Allocation calculation based on Department
     * 
     * @param employee
     *            Expense Allocation to be calculated
     * @param report
     *            Used to print the report in different formats based on the implementation i.e., Strategy Pattern
     * @param level
     *            Employee hierarchy Level of depth to be used for Expense Allocation calculation
     * @return Total Expense Allocation Amount in Currency format
     */
    public String calculateExpense(Department department, ExpenseReport report, int level) {
        if (null == department.getManager())
            throw new IllegalStateException("Department is not configured with Manager. Please configure and try again.");
        return calculateExpense(department.getManager(), report, level);
    }

    /**
     * This method is used to format the Expense Amount in Curreny format
     * 
     * @param number
     *            Expense Amount
     * @return Formatted Expense Amount in Currency format
     */
    private String getFormatedExpAlloc(BigDecimal number) {
        return NumberFormat.getCurrencyInstance().format(number);
    }

}
