/**
 * 
 */
package com.cl.codechallenge.bo;

/**
 * This is a generic interface and used to generate report in different ways based on the implementation i.e., Strategy Pattern
 * 
 * @author Aravind (netaravind@gmail.com)
 * @see com.cl.codechallenge.bo.impl.DetailedExpenseReport
 * @see com.cl.codechallenge.bo.impl.SummaryExpenseReport
 */
public interface ExpenseReport {

    /**
     * This method is used to print the Expense Allocation Report with Employee Name, his/her Expense Allocation and Running Total during the report
     * generation
     * 
     * @param expenseVO
     * @return Expense Allocation Total in a Curreny Format
     */
    String generateExpenseReport(ExpenseVO expenseVO);
}
