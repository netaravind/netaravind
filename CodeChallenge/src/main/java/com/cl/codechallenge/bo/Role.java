/**
 * 
 */
package com.cl.codechallenge.bo;

import java.math.BigDecimal;

/**
 * This is the domain object of Role which represents Role attributes such as Role Name, Default Expense Allocation for that Role
 * 
 * @author Aravind (netaravind@gmail.com)
 * @see com.cl.codechallenge.bo.RoleKey
 */
public class Role {
    private String roleName;
    private BigDecimal defaultExpenseAllocation;

    public Role(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public BigDecimal getDefaultExpenseAllocation() {
        return defaultExpenseAllocation;
    }

    public void setDefaultExpenseAllocation(BigDecimal defaultExpenseAllocation) {
        this.defaultExpenseAllocation = defaultExpenseAllocation;
    }
}
