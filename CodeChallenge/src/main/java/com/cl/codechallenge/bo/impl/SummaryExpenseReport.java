/**
 * 
 */
package com.cl.codechallenge.bo.impl;

import com.cl.codechallenge.bo.ExpenseReport;
import com.cl.codechallenge.bo.ExpenseVO;

/**
 * This implementation used to print the Expense Allocation in a summary manner. Application can use any implementation of ExpenseReport to show the
 * results i.e, This uses Strategy Pattern
 *
 * @author Aravind (netaravind@gmail.com)
 * @see com.cl.codechallenge.bo.ExpenseReport
 * @see com.cl.codechallenge.bo.impl.DetailedExpenseReport
 */
public class SummaryExpenseReport implements ExpenseReport {

    @Override
    public String generateExpenseReport(ExpenseVO expenseVO) {
        System.out.printf("Total Expense Allocation: %s %n", expenseVO.getExpenseTotal());
        return expenseVO.getExpenseTotal();
    }

}
