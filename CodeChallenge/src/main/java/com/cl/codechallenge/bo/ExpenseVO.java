/**
 * 
 */
package com.cl.codechallenge.bo;

import java.math.BigDecimal;
import java.text.NumberFormat;

/**
 * This is a Value Object used to hold the Expense Allocation calculated during the runtime and Detailed Employee information for report generation.
 * This also stores the Level which is used to restrict the Expense calculation in the recursive function
 * 
 * @author Aravind (netaravind@gmail.com)
 */
public class ExpenseVO {
    private BigDecimal expenseTotal = new BigDecimal(0);
    private StringBuilder detailedReport = new StringBuilder("");
    private int level;

    public String getExpenseTotal() {
        return NumberFormat.getCurrencyInstance().format(expenseTotal);
    }

    public BigDecimal addExpenseTotal(BigDecimal expenseTotal) {
        this.expenseTotal = this.expenseTotal.add(expenseTotal);
        return this.expenseTotal;
    }

    public String getDetailedReport() {
        return this.detailedReport.toString();
    }

    public StringBuilder setDetailedReport(String detailedReport) {
        return this.detailedReport.append(detailedReport);
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
