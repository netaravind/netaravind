package com.cl.codechallenge.bo;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import com.cl.codechallenge.bo.impl.Organization;
import com.cl.codechallenge.util.CodeChallengeUtil;

/**
 * This class used to represent the Employee domain and uses BUILDER pattern to create the object
 * 
 * @author Aravind (netaravind@gmail.com)
 * @see com.cl.codechallenge.bo.impl.Organization
 */
public class Employee {
    private Integer id;
    private String firstName;
    private String lastName;
    private String middleName;
    private BigDecimal expenseAllocation;
    private Role role;
    private Employee manager;
    private Set<Employee> directReports;

    private Employee(EmployeeBuilder employeeBuilder) {
        this.id = employeeBuilder.id;
        this.firstName = employeeBuilder.firstName;
        this.lastName = employeeBuilder.lastName;
        this.middleName = employeeBuilder.middleName;
        this.expenseAllocation = employeeBuilder.expenseAllocation;
        this.role = employeeBuilder.role;
        this.manager = employeeBuilder.manager;
    }

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public BigDecimal getExpenseAllocation() {
        return expenseAllocation;
    }

    public Role getRole() {
        return role;
    }

    public Employee getManager() {
        return manager;
    }

    public Set<Employee> getDirectReports() {
        if (null == directReports)
            directReports = new HashSet<>();
        return directReports;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setManager(Employee manager) {
        CodeChallengeUtil.validateArguments(manager);
        this.manager = manager;
        manager.getDirectReports().add(this);
    }

    public static class EmployeeBuilder {
        private Integer id;
        private String firstName;
        private String lastName;
        private String middleName;
        private BigDecimal expenseAllocation;
        private Role role;
        private Employee manager;

        public EmployeeBuilder(String firstName, String lastName, Role role) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.role = role;
        }

        public EmployeeBuilder setMiddleName(String middleName) {
            this.middleName = middleName;
            return this;
        }

        public EmployeeBuilder setExpenseAllocation(BigDecimal expenseAllocation) {
            this.expenseAllocation = expenseAllocation;
            return this;
        }

        public EmployeeBuilder setManager(Employee manager) {
            this.manager = manager;
            return this;
        }

        public Employee create() {
            CodeChallengeUtil.validateArguments(this.manager);

            this.id = CodeChallengeUtil.createEmployeeId();
            // This ternary operator used to see any custom expense limit
            // available for employee else assign default value from role
            this.expenseAllocation = (null != this.expenseAllocation && this.expenseAllocation.compareTo(BigDecimal.ZERO) > 0) ? this.expenseAllocation
                    : this.role.getDefaultExpenseAllocation();

            Employee employee = new Employee(this);
            cascadeEmployeeUpdate(employee);

            return employee;
        }

        private void cascadeEmployeeUpdate(Employee employee) {
            // Add the employee to the organization domain
            Organization.getInstance().addEmployee(employee);

            // Add the reports to the manager based on the manager id
            if (null != this.manager)
                manager.getDirectReports().add(employee);
        }

    }

}
