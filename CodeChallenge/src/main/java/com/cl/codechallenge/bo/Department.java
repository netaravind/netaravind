/**
 * 
 */
package com.cl.codechallenge.bo;

import com.cl.codechallenge.util.CodeChallengeUtil;

/**
 * This class used to represent the department domain and uses BUILDER pattern to create the object
 * 
 * @author Aravind (netaravind@gmail.com)
 * @see com.cl.codechallenge.bo.impl.Organization
 */
public class Department {
    private Integer id;
    private String name;
    private Employee manager;

    public Department(DepartmentBuilder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.manager = builder.manager;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Employee getManager() {
        return manager;
    }

    public void setManager(Employee manager) {
        this.manager = manager;
    }

    public static class DepartmentBuilder {
        private Integer id;
        private String name;
        private Employee manager;

        public DepartmentBuilder(String name) {
            this.name = name;
        }

        public DepartmentBuilder setManager(Employee manager) {
            this.manager = manager;
            return this;
        }

        public Department create() {
            this.id = CodeChallengeUtil.createDeptId();
            return new Department(this);
        }

    }

}
