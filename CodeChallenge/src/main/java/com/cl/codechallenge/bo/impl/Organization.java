/**
 * 
 */
package com.cl.codechallenge.bo.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.cl.codechallenge.bo.Department;
import com.cl.codechallenge.bo.Employee;
import com.cl.codechallenge.bo.Role;
import com.cl.codechallenge.bo.RoleKey;

/**
 * This is a main class to contain Organization level information such as Employees, Departments and Roles. This class uses Singleton design pattern
 * to restrict the object to be one. This also uses Flyweight pattern for Roles collection in such a way the role objects can be reused across the
 * application.
 * 
 * @author Aravind (netaravind@gmail.com)
 * @see com.cl.codechallenge.bo.Role
 * @see com.cl.codechallenge.bo.Employee
 * @see com.cl.codechallenge.bo.Department
 */
public class Organization {
    private String organizationName;
    private Map<Integer, Department> departments;
    private Map<Integer, Employee> employees;
    private Map<RoleKey, Role> roles;
    // Singleton based on the classloader option
    private static final Organization ORG = new Organization();

    private Organization() {
        departments = new HashMap<>();
        employees = new HashMap<>();
        setupRoles();
    }

    public static Organization getInstance() {
        return ORG;
    }

    public Department addDepartment(Department department) {
        departments.put(department.getId(), department);
        return department;
    }

    public Employee addEmployee(Employee employee) {
        employees.put(employee.getId(), employee);
        return employee;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public Collection<Employee> getAllEmployees() {
        return employees.values();
    }

    public Department getDepartment(Integer deptId) {
        return departments.get(deptId);
    }

    public Employee getEmployee(Integer employeeId) {
        return employees.get(employeeId);
    }

    /**
     * Flyweight Pattern: Roles are setup once and reused by the application
     * 
     * @param key
     * @return Role
     */
    public Role getRole(RoleKey key) {
        return roles.get(key);
    }

    private void setupRoles() {
        roles = new HashMap<RoleKey, Role>();
        // Manager Role
        Role managerRole = new Role("Manager");
        managerRole.setDefaultExpenseAllocation(BigDecimal.valueOf(300.00));
        roles.put(RoleKey.MANAGER, managerRole);

        // Developer Role
        Role devRole = new Role("Developer");
        devRole.setDefaultExpenseAllocation(BigDecimal.valueOf(1000.00));
        roles.put(RoleKey.DEVELOPER, devRole);

        // QA Role
        Role qaRole = new Role("Quality Analyst");
        qaRole.setDefaultExpenseAllocation(BigDecimal.valueOf(500.00));
        roles.put(RoleKey.QA, qaRole);

    }

}
