/**
 * 
 */
package com.cl.codechallenge.util;

import java.util.concurrent.atomic.AtomicInteger;

import com.cl.codechallenge.bo.Employee;
import com.cl.codechallenge.bo.RoleKey;
import com.cl.codechallenge.bo.impl.Organization;

/**
 * This is a Utility class to generate Employee Id, Department Id in a sequence manner.
 * 
 * @author Aravind (netaravind@gmail.com)
 *
 */
public class CodeChallengeUtil {
    // Used for Employee Id
    private static AtomicInteger employeeId = new AtomicInteger();
    // Used for Departmet Id
    private static AtomicInteger deptId = new AtomicInteger();

    public static Integer createEmployeeId() {
        return employeeId.incrementAndGet();
    }

    public static Integer createDeptId() {
        return deptId.incrementAndGet();
    }

    public static Integer getEmployeeId() {
        return employeeId.get();
    }

    public static Integer getDeptId() {
        return deptId.get();
    }

    public static void validateArguments(Employee manager) {
        if (null != manager && (manager.getRole() != Organization.getInstance().getRole(RoleKey.MANAGER)))
            throw new IllegalArgumentException("Employees with Manager Role can have reports. Please check and try again.");
    }

}
