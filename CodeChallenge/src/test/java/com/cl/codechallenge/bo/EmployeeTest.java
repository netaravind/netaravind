/**
 * 
 */
package com.cl.codechallenge.bo;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runners.MethodSorters;

import com.cl.codechallenge.bo.impl.Organization;

/**
 * This test class tests various operations, scenarios in the Employee
 * 
 * @author Aravind (netaravind@gmail.com)
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EmployeeTest {

    private static Organization organization;
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public static void setup() {
        organization = Organization.getInstance();
    }

    @Test
    public void addEmployee() {
        Employee manager1 = new Employee.EmployeeBuilder("Manager First Name", "Manager Last Name", organization.getRole(RoleKey.MANAGER)).create();
        Employee developer1 = new Employee.EmployeeBuilder("Dev First Name", "Dev Last Name", organization.getRole(RoleKey.DEVELOPER)).setManager(
                manager1).create();
        Employee qa1 = new Employee.EmployeeBuilder("QA First Name", "QA Last Name", organization.getRole(RoleKey.QA))
                .setExpenseAllocation(BigDecimal.valueOf(750.00)).setMiddleName("QA Middle Name").setManager(manager1).create();

        assertEquals(BigDecimal.valueOf(300.0), manager1.getExpenseAllocation());
        assertEquals(BigDecimal.valueOf(1000.0), developer1.getExpenseAllocation());
        assertEquals(BigDecimal.valueOf(750.0), qa1.getExpenseAllocation());
        assertEquals("Manager First Name", manager1.getFirstName());
        assertEquals("QA Middle Name", qa1.getMiddleName());
        assertEquals("Dev Last Name", developer1.getLastName());
        assertSame(manager1, developer1.getManager());
        assertSame(organization.getRole(RoleKey.QA), qa1.getRole());
        assertEquals(2, manager1.getDirectReports().size());
    }

    @Test
    public void addEmployeeManager() {
        Employee manager1 = new Employee.EmployeeBuilder("Manager First Name", "Manager Last Name", organization.getRole(RoleKey.MANAGER)).create();
        Employee qa1 = new Employee.EmployeeBuilder("QA First Name", "QA Last Name", organization.getRole(RoleKey.QA)).create();
        qa1.setManager(manager1);
        assertSame(manager1, qa1.getManager());
        assertTrue(manager1.getDirectReports().contains(qa1));
    }

    @Test
    public void addEmployee_Exception() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Employees with Manager Role can have reports. Please check and try again.");
        Employee developerA = new Employee.EmployeeBuilder("Dev First Name1", "Dev Last Name1", organization.getRole(RoleKey.DEVELOPER)).create();
        new Employee.EmployeeBuilder("Dev First Name", "Dev Last Name", organization.getRole(RoleKey.DEVELOPER)).setManager(developerA).create();
    }

    @Test
    public void addEmployee_Exception1() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Employees with Manager Role can have reports. Please check and try again.");
        Employee developerA = new Employee.EmployeeBuilder("Dev First Name1", "Dev Last Name1", organization.getRole(RoleKey.DEVELOPER)).create();
        Employee developerB = new Employee.EmployeeBuilder("Dev First Name", "Dev Last Name", organization.getRole(RoleKey.DEVELOPER)).create();
        developerB.setManager(developerA);
    }

}
