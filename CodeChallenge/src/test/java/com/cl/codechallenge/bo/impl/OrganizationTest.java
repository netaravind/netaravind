/**
 * 
 */
package com.cl.codechallenge.bo.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.cl.codechallenge.bo.Department;
import com.cl.codechallenge.bo.Employee;
import com.cl.codechallenge.bo.Role;
import com.cl.codechallenge.bo.RoleKey;
import com.cl.codechallenge.util.CodeChallengeUtil;

/**
 * This class tests Organization operations with various scenarios
 * 
 * @author Aravind (netaravind@gmail.com)
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OrganizationTest {

    private static Organization organization;

    @BeforeClass
    public static void setup() {
        organization = Organization.getInstance();
    }

    /**
	 * 
	 */
    @Test
    public void getInstanceTest() {
        organization.setOrganizationName("CenturyLink");
        Organization organization1 = Organization.getInstance();
        assertSame("Singleton implementation of Organization failed", organization, organization1);
        assertEquals("CenturyLink", organization1.getOrganizationName());
    }

    @Test
    public void getRoles() {
        Role managerRole = organization.getRole(RoleKey.MANAGER);
        Role devRole = organization.getRole(RoleKey.DEVELOPER);
        Role qaRole = organization.getRole(RoleKey.QA);

        assertEquals(BigDecimal.valueOf(300.0), managerRole.getDefaultExpenseAllocation());
        assertEquals("Manager", managerRole.getRoleName());

        assertEquals(BigDecimal.valueOf(1000.0), devRole.getDefaultExpenseAllocation());
        assertEquals("Developer", devRole.getRoleName());

        assertEquals(BigDecimal.valueOf(500.0), qaRole.getDefaultExpenseAllocation());
        assertEquals("Quality Analyst", qaRole.getRoleName());

    }

    @Test
    public void addDepartment() {
        Employee manager = new Employee.EmployeeBuilder("First Name", "Last Name", organization.getRole(RoleKey.MANAGER)).create();
        Department department = new Department.DepartmentBuilder("Sales").setManager(manager).create();
        organization.addDepartment(department);
        assertEquals(CodeChallengeUtil.getDeptId(), department.getId());
        assertSame(department, organization.getDepartment(department.getId()));
    }

    @Test
    public void addEmployee() {
        Employee managerA = new Employee.EmployeeBuilder("Manager First Name", "Manager Last Name", organization.getRole(RoleKey.MANAGER)).create();
        Employee developerA = new Employee.EmployeeBuilder("Dev First Name", "Dev Last Name", organization.getRole(RoleKey.DEVELOPER)).setManager(
                managerA).create();
        assertEquals(BigDecimal.valueOf(300.0), managerA.getExpenseAllocation());
        assertEquals(BigDecimal.valueOf(1000.0), developerA.getExpenseAllocation());
        assertEquals(CodeChallengeUtil.getEmployeeId(), developerA.getId());
        assertEquals(1, managerA.getDirectReports().size());
    }

    @Test
    public void getAllEmployees() {
        List<Employee> allEmployees = new ArrayList<Employee>(organization.getAllEmployees());
        assertTrue(allEmployees.size() > 0);
    }

    @Test
    public void getEmployee() {
        Employee employee = organization.getEmployee(2);
        assertNotNull(employee);
        assertEquals(new Integer(2), employee.getId());
    }

}
