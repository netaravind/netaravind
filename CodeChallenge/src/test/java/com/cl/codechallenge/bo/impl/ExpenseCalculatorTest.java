/**
 * 
 */
package com.cl.codechallenge.bo.impl;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.text.NumberFormat;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestName;
import org.junit.runners.MethodSorters;

import com.cl.codechallenge.bo.Department;
import com.cl.codechallenge.bo.Employee;
import com.cl.codechallenge.bo.RoleKey;

/**
 * This test class tests various scenarios in the Expense Report calculation such as Employee (Manager) object alone, contains DirectReport, Direct
 * Reports in Hierarchy manner i.e., N level of Depth, Calculate Expense report based on Department, Department without a Manager etc
 * 
 * @author Aravind (netaravind@gmail.com)
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ExpenseCalculatorTest {

    private static Organization organization;
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Rule
    public TestName name = new TestName();

    @BeforeClass
    public static void setup() {
        organization = Organization.getInstance();
    }

    @Before
    public void printMethodName() {
        System.out.println("Method: " + name.getMethodName());
    }

    @After
    public void formatConsole() {
        System.out.print("\n");
        System.out.println(new String(new char[125]).replace("\0", "-"));
    }

    @Test
    public void addEmployee() {
        Employee manager1 = new Employee.EmployeeBuilder("Manager First Name", "Manager Last Name", organization.getRole(RoleKey.MANAGER)).create();
        new Employee.EmployeeBuilder("Dev First Name", "Dev Last Name", organization.getRole(RoleKey.DEVELOPER)).setManager(manager1).create();
        new Employee.EmployeeBuilder("QA First Name", "QA Last Name", organization.getRole(RoleKey.QA)).setExpenseAllocation(new BigDecimal(750.00))
                .setMiddleName("QA Middle Name").setManager(manager1).create();
        assertEquals(getAmount(2050), new ExpenseCalculator().calculateExpense(manager1, new SummaryExpenseReport(), 0));

    }

    /**
     * This method used to demonstrate the example scenario given in the Coding Challenge document.
     */
    @Test
    public void test_ExampleScenario() {
        Employee manager1 = new Employee.EmployeeBuilder("Manager1 First Name", "Manager1 Last Name", organization.getRole(RoleKey.MANAGER)).create();
        Employee manager2 = new Employee.EmployeeBuilder("Manager2 First Name", "Manager2 Last Name", organization.getRole(RoleKey.MANAGER))
                .setManager(manager1).create();
        new Employee.EmployeeBuilder("Dev First Name", "Dev Last Name", organization.getRole(RoleKey.DEVELOPER)).setManager(manager2).create();
        new Employee.EmployeeBuilder("QA First Name", "QA Last Name", organization.getRole(RoleKey.QA)).setMiddleName("QA Middle Name")
                .setManager(manager2).create();
        assertEquals(getAmount(2100), new ExpenseCalculator().calculateExpense(manager1, new SummaryExpenseReport(), 0));

    }

    @Test
    public void addNestedEmployees() {
        Employee manager1 = new Employee.EmployeeBuilder("Manager1 First Name", "Manager1 Last Name", organization.getRole(RoleKey.MANAGER)).create();
        new Employee.EmployeeBuilder("Dev1 First Name", "Dev1 Last Name", organization.getRole(RoleKey.DEVELOPER)).setManager(manager1).create();
        // This employee has custom Expense Allocation from his/her role
        Employee qa1 = new Employee.EmployeeBuilder("QA1 First Name", "QA1 Last Name", organization.getRole(RoleKey.QA))
                .setExpenseAllocation(new BigDecimal(750.00)).setMiddleName("QA Middle Name").setManager(manager1).create();

        // Manager2 reports to Manager1 and Manager2 has reports i.e., Nested reports from Manager1
        Employee manager2 = new Employee.EmployeeBuilder("Manager2 First Name", "Manager2 Last Name", organization.getRole(RoleKey.MANAGER))
                .setManager(manager1).create();
        new Employee.EmployeeBuilder("Dev2 First Name", "Dev2 Last Name", organization.getRole(RoleKey.DEVELOPER)).setManager(manager2).create();
        new Employee.EmployeeBuilder("Dev3 First Name", "Dev3 Last Name", organization.getRole(RoleKey.DEVELOPER)).setManager(manager2).create();

        ExpenseCalculator expenseCalculator = new ExpenseCalculator();
        // Assert Manager1 Expense Allocation and his/her reports i.e., this will include Manager2 and his report.
        // This also uses the DetailedReport format to show the results i.e., Example of Strategy Pattern
        assertEquals(getAmount(4350), expenseCalculator.calculateExpense(manager1, new DetailedExpenseReport(), 2));

        // Assert Manager2 and his/her Direct Reports. Summary Report format is used to show the results
        assertEquals(getAmount(2300), expenseCalculator.calculateExpense(manager2, new SummaryExpenseReport(), 0));

        // Assert QA1 Expense Allocation i.e., our logic will calculate Expense Allocation of employee as well
        assertEquals(getAmount(750), expenseCalculator.calculateExpense(qa1, new SummaryExpenseReport(), 0));

    }

    @Test
    public void test_N_LevelInNestedEmployees() {
        Employee manager1 = new Employee.EmployeeBuilder("Manager1 First Name", "Manager1 Last Name", organization.getRole(RoleKey.MANAGER)).create();
        new Employee.EmployeeBuilder("Dev1 First Name", "Dev1 Last Name", organization.getRole(RoleKey.DEVELOPER)).setManager(manager1).create();
        new Employee.EmployeeBuilder("QA1 First Name", "QA1 Last Name", organization.getRole(RoleKey.QA)).setMiddleName("QA Middle Name")
                .setManager(manager1).create();

        // Manager2 reports to Manager1 and Manager2 has reports i.e., Nested reports from Manager1
        Employee manager2 = new Employee.EmployeeBuilder("Manager2 First Name", "Manager2 Last Name", organization.getRole(RoleKey.MANAGER))
                .setManager(manager1).create();
        new Employee.EmployeeBuilder("Dev2 First Name", "Dev2 Last Name", organization.getRole(RoleKey.DEVELOPER)).setManager(manager2).create();
        new Employee.EmployeeBuilder("Dev3 First Name", "Dev3 Last Name", organization.getRole(RoleKey.DEVELOPER)).setManager(manager2).create();

        // Manager3 reports to Manager2 i.e., 2nd level
        Employee manager3 = new Employee.EmployeeBuilder("Manager3 First Name", "Manager3 Last Name", organization.getRole(RoleKey.MANAGER))
                .setManager(manager2).create();
        // QA2 reports to Manager3 i.e., 3rd level
        new Employee.EmployeeBuilder("QA2 First Name", "QA2 Last Name", organization.getRole(RoleKey.QA)).setManager(manager3).create();

        ExpenseCalculator expenseCalculator = new ExpenseCalculator();

        // Level 1 Assertion i.e., Manager1, his/her Direct Reports, Manager2 alone
        assertEquals(getAmount(2100), expenseCalculator.calculateExpense(manager1, new DetailedExpenseReport(), 1));

        // Level 2 Assertion i.e., Manager1, his/her Direct Reports, Manager2, his/her Direct Reports and Manager 3 alone
        assertEquals(getAmount(4400), expenseCalculator.calculateExpense(manager1, new DetailedExpenseReport(), 2));

        // Level 2 Assertion i.e., Manager1, his/her Direct Reports, Manager2, his/her Direct Reports and Manager 3 and his/her Direct Reports
        assertEquals(getAmount(4900), expenseCalculator.calculateExpense(manager1, new DetailedExpenseReport(), 3));

        // Level 0 i.e., No Levels and print all
        assertEquals(getAmount(4900), expenseCalculator.calculateExpense(manager1, new DetailedExpenseReport(), 0));

    }

    @Test
    public void addDepartment() {
        Employee manager1 = new Employee.EmployeeBuilder("Manager First Name", "Manager Last Name", organization.getRole(RoleKey.MANAGER)).create();
        new Employee.EmployeeBuilder("Dev First Name", "Dev Last Name", organization.getRole(RoleKey.DEVELOPER)).setManager(manager1).create();
        new Employee.EmployeeBuilder("QA First Name", "QA Last Name", organization.getRole(RoleKey.QA)).setManager(manager1).create();
        Department department = new Department.DepartmentBuilder("IT").setManager(manager1).create();
        assertEquals(getAmount(1800), new ExpenseCalculator().calculateExpense(department, new SummaryExpenseReport(), 0));

    }

    @Test
    public void addDepartment_Exception() {
        thrown.expect(IllegalStateException.class);
        thrown.expectMessage("Department is not configured with Manager. Please configure and try again.");
        Department department = new Department.DepartmentBuilder("IT").create();
        assertEquals(getAmount(1800), new ExpenseCalculator().calculateExpense(department, new SummaryExpenseReport(), 0));
    }

    private String getAmount(double number) {
        return NumberFormat.getCurrencyInstance().format(number);
    }

}
